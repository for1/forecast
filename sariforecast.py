# !/usr/bin/env python
# _*_ UTF-8 _*_
import pandas as pd
import tensorflow as tf
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from matplotlib import pyplot
from matplotlib import animation

if __name__ == '__main__':

    model = tf.keras.models.load_model('sars_analysis.model')

    dataset = pd.read_csv('forecast.csv', header=0, index_col=0)
    #print(dataset)
    # 数据预处理：
    values = dataset.values
    valuesresult=dataset.values
    for i in range(1,60):
    #print(values)
    # ensure all data is float
        values = values.astype('float32')
        # 数据归一化
        scaler = MinMaxScaler(feature_range=(0, 1))
        values = scaler.fit_transform(values)
        train_X = values.reshape((values.shape[0], 1, values.shape[1]))
        # 数据预测：
        yhat = model.predict(train_X)
        # 数据还原：
        test_X = train_X.reshape((train_X.shape[0], train_X.shape[2]))
        # invert scaling for forecast concatenate：数据拼接
        #inv_yhat = concatenate((yhat, test_X[:, 1:]), axis=1)
        # 3、是将标准化后的数据转换为原始数据：
        inv_yhat = scaler.inverse_transform(yhat)
        #inv_yhat = inv_yhat[:, 0]
        values=np.vstack([valuesresult, inv_yhat[-1]])
        #人数不可能小于0
        values=np.maximum(values, 0)
        valuesresult=values

    print(valuesresult)




    #绘图
    fig, ax = pyplot.subplots()
    xdata, ydata = [], []
    x1data, y1data = [], []
    ln, = ax.plot([], [], 'r-', animated=False)
    ln1, = ax.plot([], [], 'r-', animated=False)
    def init():
        ax.set_xlim(0, 110)  # 设置x轴的范围
        ax.set_ylim(-10, 100000)# 设置y轴的范围
        return ln,ln1  # 返回曲线


    def update(n):
        xdata.append(n)  # 将每次传过来的n追加到xdata中
        x1data.append(n)
        ydata.append(valuesresult[n, 0])
        y1data.append(valuesresult[n, 4])
        ln.set_data(xdata, ydata)  # 重新设置曲线的值
        ln1.set_data(x1data, y1data)
        return ln,ln1


    ani = animation.FuncAnimation(fig, update, frames=93,  # 这里的frames在调用update函数是会将frames作为实参传递给“n”
                       init_func=init, blit=True)
    ani.save('line.gif', dpi=80, writer='imagemagick')
    pyplot.show()
    # groups = [0, 4]
    # i = 1
    # # plot each column
    # pyplot.rcParams['font.family'] = 'STSong'
    # fig=pyplot.figure()
    # for group in groups:
    #     fig, ax= pyplot.subplot(len(groups), 1, i)
    #     ln,=ax.plot(valuesresult[:, group], marker='o',
    #                 label= '趋势',animated=False)
    #
    #
    #     def init():
    #         ax.set_xlim(0, max(valuesresult[:, group]) * 1.8)
    #         return ln,
    #
    #
    #     def update(frame):
    #         ln.set_data(valuesresult.index, valuesresult[:, group])
    #         return ln,
    #
    #     pyplot.xlabel('时间')
    #     pyplot.ylabel('人数')
    #     for a, b in zip(range(1,len(valuesresult)+1), valuesresult[:, group]):
    #         if (b == min(valuesresult[:, group])) or (b == max(valuesresult[:, group])):
    #             pyplot.text(a, b, (b), ha='center', va='bottom', fontsize=10)
    #     pyplot.legend()
    #    # pyplot.ylim(0, max(valuesresult[:, group]) * 1.8)
    #     i += 1
    #     anim = animation.FuncAnimation(fig, update,
    #                                    frames=200, interval=20, blit=True)
    # pyplot.show()