##数据整理
from pandas import read_csv
import numpy as np
from datetime import datetime

# load data
def parse(x):
    return datetime.strptime(x, '%Y/%m/%d')

dataset = read_csv('sarsdata.csv', index_col=1, date_parser=parse)
#dataset.drop('序号', axis=1, inplace=True)
#dataset = read_csv('saridata.csv',)
dataset.drop('序号', axis=1, inplace=True)
# manually specify column names
dataset.columns = ['确诊', '治愈', '危重', '死亡', '疑似']
dataset.index.name = '时间'
# mark all NA values with 0
dataset['疑似'].fillna(0, inplace=True)
# print data
print(dataset)
# save to file
dataset.to_csv('sarsdataout.csv')
